{
   DCAL program Space is intended to be a simple introduction to DCAL
   programming.  It is an example of a parametric macro which requests
   input, allows the changing of one or more parameters, in this case
   the width of the Space, and adds entities which describe the geometry
   to your drawing database.

   Although this macro is decidedly simple, it embodies many of the basic
   principles used in writing much larger and more complex DCAL programs.
   For example, the principles used here are virtually identical to the
   way in which the DCAD_AEC macro is written.
}
{----------------------------------------------------------------------------}

library SpaceSelect;

uses
  Dialogs,
  Forms,
  Windows,
  DateUtils, SysUtils,
  Process in 'Process.pas',
  UConstants in '..\..\Header Files\UConstants.pas',
  UDCLibrary in '..\..\Header Files\UDCLibrary.pas',
  UInterfaces in '..\..\Header Files\UInterfaces.pas',
  UInterfacesRecords in '..\..\Header Files\UInterfacesRecords.pas',
  URecords in '..\..\Header Files\URecords.pas',
  UVariables in '..\..\Header Files\UVariables.pas',
  Version in 'Version.pas';

{$E dmx}
{$R *.res}

   procedure GetColor(var Color : longint);
   var
      ClrD : TColorDialog;
   begin
      ClrD := TColorDialog.Create(Application);
      ClrD.Color := Color;
      ClrD.Options := [cdFullOpen];
      if ClrD.Execute then Color := RGBtoDCADRGB(ClrD.Color);
      ClrD.Free;
   end;


type
   SpaceL = record
      state:    asint;
      wallwidth: double;
      openingwidth : double;
      fillcolor: longint;
      case byte of
         0: (getp: getpointarg);
         1: (getd: getdisarg);
   end;
   PSpaceL = ^SpaceL;

   function Space_Main(act : action; pl, pargs : Pointer) : wantType;
   var
      retval : asint;
      l :      PSpaceL;
      mode : mode_type;
      ent : entity;
      atr : attrib;
      expiry : TDate;
   begin
      l := PSpaceL(pl);
      if act = aagain then begin
         case l.state of
            1 :
            begin
               if l.getp.Result = res_escape then begin
                  case l.getp.key of
                     f3 : GetColor(l.fillcolor);
                     f4 : GetColorIndex(l.fillcolor);
                     f6 : l.state := 2;
                     f7 : l.state := 3;
                     s0 : l.state := 0;
                  end;
               end
               else if l.getp.Result = res_normal then begin
                  mode_init1 (mode);
                  mode_enttype (mode, entlin);
                  if ent_near (ent, l.getp.curs.x, l.getp.curs.y, mode, true) then begin
                    mode_1lyr (mode, ent.lyr);
                    if ProcessRoom (mode, ent, l.wallwidth, l.openingwidth) then begin
                      atr := InitSolidFillAttribute (l.fillcolor, 0, 0);
                      atr_add2ent (ent, atr);
                      ent_draw (ent, drmode_white);
                    end;
                  end;
               end;
            end;
            2, 3 : l.state := 1;
            else
               l.state := 0;
         end;
      end;
      if act = afirst then begin
        l.state     := 1;
        l.fillcolor := RGB(255, 255, 255);
        l.wallwidth  := PGSaveVar.wallwidth;  { first time initialize arrwidth to 2 feet }
        l.openingwidth := 5000;
        Expiry := EncodeDateTime (2019, 6, 30, 23, 59, 59, 999);
        if Now > Expiry then begin
          l.state := 0;
          MessageDlg ('This macro has expired.' + sLineBreak + 'Please contact dhSoftware (dhsoftware1@gmail.com) if you wish to continue using it.', mtError, [mbOK], 0);
        end
        else if Now > IncDay (Expiry, -30) then
          MessageDlg ('This macro will expire on ' + datetostr (Expiry) + sLineBreak + sLineBreak +
                      'Please contact dhSoftware (dhsoftware1@gmail) if you wish to use it beyond this date', mtWarning, [mbOK], 0);
      end
      else if act = alsize then begin
         SetLocalSize(sizeof(l^));
      end;
      if act <> alsize then begin
         wrterr('Space Select - Version ' + FileVersion + '  (c)David Henderson 2018.  Expires ' + datetostr(Expiry), true);
         case l.state of
            1 :
            begin
               wrtlvl('Space');        { the 8 character identifier on the message line }
               lblsinit;               { initialize the function key labels }
               lblset (3, 'Custom Colour');
               lblset (4, 'Pallete Colour');
               lblset (6, 'Wall Width');
               lblset (7, 'Max Opening');
               lblset(20, 'Exit');
               lblson;                 { turn on the function keys }
               { prompt the user for what to do }
               wrtmsg('Enter a point on the room outline.');
               getpoint(l.getp, retval);
            end;
            2 :
            begin
               wrtmsg('Enter Wall width: ');
               getdis(l.wallwidth, l.getd, retval);
            end;
            3 :
            begin
               wrtmsg('Enter max opening width: ');
               getdis(l.openingwidth, l.getd, retval);
            end;
            else
               retval := XDone;
         end;
      end;
      Result := retval;
   end;

   function Main(dcalstate : asint; act : action; pl, pargs : Pointer) : wantType; stdcall;
   begin
      case dcalstate of
         XDcalStateBegin : Result := Space_main(act, pl, pargs);
         else
            Result := XDone;{Necessary}
      end;
   end;


exports
   Main;{This is the entry function that will be called by DataCAD. All DCAL Dlls
   will have this function.}

begin

end.
